#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define True 0==0

int main() {
    
    int pos, pos_real;
    int numero_de_tentativas, numero_de_arvores = 50;
    int jogador, rodada = 0;

    system("clear");

    while(True) {

        numero_de_tentativas = 5;
        jogador = rodada % 2 + 1;

        printf("Ok, jogador %i, escolha a arvore onde o Thanos irá se esconder: ", jogador);
        scanf("%i", &pos_real);

        if(jogador == 1) jogador++;
        else jogador--;

        while(numero_de_tentativas != 0) {
            
            printf("Jogador %i, digite qual arvore você deseja atingir: ", jogador);
            scanf("%i", &pos);

            if(pos < 1 || pos > numero_de_arvores)
                printf("Tente um numero entre 1 e 50\n");

            if(pos < pos_real){
                printf("Estou mais para a direita\n");
                numero_de_tentativas--;
            }
            if(pos > pos_real){
                printf("Estou mais para a esquerda\n");
                numero_de_tentativas--;
            }
            if(pos == pos_real) {
                printf("\nParabéns, Jogador %i! Você acertou o Thanos\n\n", jogador);
                rodada++;
                break;
            }
            
            rodada++;

            if(numero_de_tentativas == 0) 
                printf("\nVocê não consegiu acertar o Thanos, Jogador %i\n\n", jogador);

        }

    }

    return 0;

}

